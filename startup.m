function startup()
%% STARTUP starts this project



%% File information
% Author: Philipp Tempel <philipp.tempel@isw.uni-stuttgart.de>
% Date: 2022-08-02
% Changelog:
%   2022-08-02
%       * Ensure dependencies and paths are added in the order given
%   2022-03-14
%       * Split repository into tooling repo and workspace repo, thus update
%       function to reflect new layout
%   2020-06-11
%       * Add initialization of figure styles, diaries, format, random number
%       generator, warnings, last workspace
%       * Make standalone tooling project and ensure that it can be used as
%       start directory of MATLAB
%   2018-05-16
%       * Move the installation of the plot styles to a separate file
%   2018-05-14
%       * Add installation of plot styles to MATLAB's `prefdir()/ExportSetup`
%       directory
%   2018-04-29
%       * Move all path definitions to `pathdef.m`
%   2017-03-12
%       * Initial release



%% Bootstrap

% Set time zone
setenv('TZ', 'Europe/Zurich');

% Check file `MPATHDEF` exists
if 2 ~= exist('mpathdef.m', 'file')
  error('File `mpathdef.m` does not exist. Please read the README before continuing.');
end
% Get path to PHILIPPTEMPEL/MATLAB-TOOLING
p = mpathdef();
% Store current directory
cwd = pwd();
coCwd = onCleanup(@() cd(cwd));
% Change directory to PHILIPPTEMPEL/MATLAB-TOOLING
cd(p);
startup();
cd(cwd);

% Finally, having `MATLABPROJECT` available, we can fully activate this project
addpath(MatlabProject.this());



%% Configure MATLAB: path and diaries

% Rotate diary
diaryrotate();
% Set diary path
diary(fullfile(userpath(), 'log', 'commands.log'));



%% Init MATLAB

% Time right now as date vector
% c = [year, month, day, hour, minute, second]
dvnow = datevec(datetime('now'));

% Set random number generator to a set or pre-defined options
setrng();

% Initialize random state
rng(sum(100 * dvnow), 'v5uniform');
rng(sum(100 * dvnow), 'v5normal');

% Command window format
format('compact');
format('longG');

% Default recursion limits
set(0, 'RecursionLimit', 50);

% Select solarized style depending on current hour of the day (at night i.e., between 8pm and 8am we choose 'dark', otherwise 'light'
if dvnow(4) < 8 || 20 <= dvnow(4)
  setupSolarized('dark');
  
else
  setupSolarized('light');
  
end

% Fix some nasty auto-indenting-and-removing-whitespace in MATLAB >R2022a
if ~isMATLABReleaseOlderThan('R2021b')
  try
    s = settings();
    s.matlab.editor.indent.RemoveAutomaticWhitespace.PersonalValue = 0;
    
  catch me
    warning(me.identifier, '%s', me.message);
    
  end
  
end

% Configuration of symbolic toolbox, if installed
if isfunction(@sympref)
  sympref('PolynomialDisplayStyle', 'ascend');
  
end



%% Setting up figures

%%% Default figure properties
set( ...
    groot() ...
  , 'DefaultFigurePaperType'      , 'A4' ...
  ... , 'DefaultFigurePaperSize'    , [ 21.0 , 29.7 ] ...
  , 'DefaultFigurePaperUnits'     , 'normalized' ...
  , 'DefaultFigurePaperPosition'  , [ 0.05 , 0.05 , 0.95 , 0.95 ] ...
  , 'DefaultFigureWindowStyle'    , 'normal' ....
);

%%% Default text properties
set( ...
    groot() ...
  , 'DefaultTextFontSize', 14 ...
);

%%% Default UI Control properties
set( ...
    groot() ...
  , 'DefaultUicontrolFontSize', 8 ...
);

%%% Default axes properties
set( ...
    groot() ...
  , 'DefaultAxesBox'                    , 'on' ...
  , 'DefaultAxesFontSize'               , 16 ...
  , 'DefaultAxesLineStyleOrder'         , '-|--|-.|:' ...
  , 'DefaultAxesLinewidth'              , 1.5 ...
  , 'DefaultAxesTitleFontSizeMultiplier', 1.00 ...
  , 'DefaultAxesLabelFontSizeMultiplier', 1.00 ...
  , 'DefaultAxesXGrid', 'on' ...
  , 'DefaultAxesYGrid', 'on' ...
  , 'DefaultAxesZGrid', 'on' ...
);

%%% Default tiled layout properties
set( ...
    groot() ...
  , 'DefaultTiledLayoutPadding'     , 'tight' ...
  , 'DefaultTiledLayoutTileSpacing' , 'tight' ......
);

%%% Default graphics primitives properties
set( ...
    groot() ...
  , 'DefaultLineLinewidth'  , 1.5 ...
  , 'DefaultPatchLinewidth' , 1.5 ...
  , 'DefaultQuiverLinewidth', 1.5 ...
  , 'DefaultStairLinewidth' , 1.5 ...
);

%%% Default interpreters
set( ...
    groot() ...
  , 'DefaultAxesTickLabelInterpreter'     , 'latex' ...
  , 'DefaultBubblelegendInterpreter'      , 'latex' ...
  , 'DefaultColorbarTickLabelInterpreter' , 'latex' ...
  , 'DefaultConstantlineInterpreter'      , 'latex' ...
  , 'DefaultGraphplotInterpreter'         , 'latex' ...
  , 'DefaultLegendInterpreter'            , 'latex' ...
  , 'DefaultPolaraxesTickLabelInterpreter', 'latex' ...
  , 'DefaultTextInterpreter'              , 'latex' ...
  , 'DefaultTextarrowshapeInterpreter'    , 'latex' ...
  , 'DefaultTextboxshapeInterpreter'      , 'latex' ...
);



%% Setting up handy debugging options

% And some other values
recycle('off');
warning('off', 'verbose');
warning('on', 'all');
warning('on', 'backtrace');



%% Last steps

publishplotstyles(fullfile(fileparts(mfilename('fullpath')), 'plotstyles'));

% % Get all mat files in the subdirectory
% stFiles = dir(fullfile(chBase, 'workspace', 'ws_*.mat'));
% 
% % If we have found some files
% if numel(stFiles) > 0
%   [~, idxDates] =  sort([stFiles(:).datenum]);
%   try
%     evalin('base', sprintf('load(''%s'');', fullfile(stFiles(idxDates(end)).folder, stFiles(idxDates(end)).name)));
%   catch
%   end
% end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
