function p = mpathdef()
%% MPATHDEF Provide MATLAB path definition
%
% Outputs:
%
%   P                       Path to directory of PHILIPPTEMPEL/MATLAB-TOOLING.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% MPATHDEF()
narginchk(0, 0);

% MPATHDEF(___)
% P = MPATHDEF(___)
nargoutchk(0, 1);



%% Algorithm

% Change this line to point to where you have installed
% philipptempel/matlab-tooling to.
p = fullfile('/path', 'to', 'philipptempel', 'matlab-tooling');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
