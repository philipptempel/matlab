function solarized(varargin)
%% SOLARIZED 



%% File information
% Author: Philipp Tempel <matlab@philipptempel.me>
% Date: 2021-12-14
% Changelog:
%   2021-12-14
%       * Update email address of Philipp Tempel
%   2020-06-11
%       * Initial release



%% Do your code magic here
% just pass all arguments down to the `setupSolarized` function
setupSolarized(varargin{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
