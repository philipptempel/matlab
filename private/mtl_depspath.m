function p = mtl_depspath()
%% MTL_DEPSPATH returns the path definiton for this project's dependencies
%
% P = MTL_DEPSPATH()
%
% Outputs:
%
%   P                   Cell array of paths to automatically load



%% File information
% Author: Philipp Tempel <philipp.tempel@isw.uni-stuttgart.de>
% Date: 2022-08-02
% Changelog:
%   2022-08-02
%       * Change where MATLAB-TOOLING project is loaded from
%   2022-03-14
%       * Initial release



%% Parse arguments



%% Algorithm

% Base directory
b = fullfile(fileparts(mfilename('fullpath')), '..', 'dependencies');

% Parsed base directory
chPath = char(java.io.File(b).getCanonicalPath());

% Home directory
h = getenv('HOME');

% All paths to add
p = { ...
    genpath(fullfile(h, 'projects', 'matlab-tooling', 'dev')) ...
  , genpath(fullfile(h, 'projects', 'matlab-tooling', 'mex')) ...
  , genpath(fullfile(h, 'projects', 'matlab-tooling', 'src')) ...
  , fullfile(chPath, 'exportfig') ...
  , fullfile(chPath, 'jsonlab') ...
  , fullfile(chPath, 'matlab2tikz', 'src') ...
  , fullfile(chPath, 'solarized-matlab') ...
};

% Remove empty paths
p(cellfun(@isempty, p)) = [];

% Process paths
p = cellfun( ...
    @(ip) strjoin( ...
        cellfun( ...
            @(p) char(java.io.File(p).getCanonicalPath()) ...
          , strsplit(strip(ip, 'both', pathsep()), pathsep()) ...
          , 'UniformOutput', false ...
        ) ...
      , pathsep() ...
    ) ...
  , p ...
  , 'UniformOutput', false ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
