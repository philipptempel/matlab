# MATLAB Workspace

This is my personal MATLAB workspace repository.
It is the startup directory of all of my MATLAB installations.

## Dependencies

This project directly depends on the following other projects/packages:

1. MATLAB Tooling by Philipp Tempel: https://gitlab.com/philipptempel/matlab-tooling

This project makes use of the following packages

1. atlmany's exportfig: https://github.com/altmany/export_fig.git
1. fangq's jsonlab: https://github.com/fangq/jsonlab.git
1. matlab2tikz: https://github.com/matlab2tikz/matlab2tikz.git
1. benhager's solarized-matlab: https://github.com/benhager/solarized-matlab.git

These packages are all set up as git submodules so that their version is fixed ensuring compatability.

## Installation

1. Clone the main dependency
    ```bash
    $ git clone --recursive https://github.com/philipptempel/matlab-tooling.git
    ```
1. Clone this repository
    ```bash
    $ git clone --recursive https://github.com/philipptempel/matlab.git
    ```
1. Copy the file `mpathdef.template.m` to `mpathdef.m` and change its content to point to the location where `matlab-tooling` has been installed to

## Usage

It will be as simple as starting MATLAB into this directory or setting MATLAB's startup directory to this project's directory.
Alternatively, you may also change directories into this project and call the `startup()` function

```matlab
>> cd('path/to/philipptempel/matlab');
>> startup();
```
