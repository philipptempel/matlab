function finish()
%% FINISH shuts down the project



%% File information
% Author: Philipp Tempel <philipp.tempel@isw.uni-stuttgart.de>
% Date: 2022-08-02
% Changelog:
%   2022-08-02
%       * Ensure dependencies and paths are removed in reverse of the order
%       given
%   2022-03-14
%       * Use `ENSUREDIR` when saving the workspace
%       * Split repository into tooling repo and workspace repo, thus update
%       function to reflect new layout
%   2022-01-31
%       * Fix removing of paths in incorrect order
%   2021-12-22
%       * Fix order in which paths are removed to from-back-to-front
%   2021-05-11
%       * Remove paths added to MATLAB's search path
%   2017-03-12
%       * Initial release



%% Algorithm

try

  % Where to save the current workspace to
  chWorkspacePath = ensuredir(fullfile(fileparts(mfilename('fullpath')), 'workspace'));

  % Save the current workspace to a file in this file's directory
  evalin('base', sprintf('save(''%s'');', fullfile(chWorkspacePath, sprintf('ws_%s.mat', datestr(now, 'yyyymmddThhMMss')))));

catch me
  warning(me.identifier, '%s', me.message);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
